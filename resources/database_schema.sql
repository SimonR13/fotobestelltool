-- DROP 
-- SEQUENCE
drop sequence FOTOTOOLSCHEMA.hibernate_sequence restrict;
-- CONSTRAINTS
alter table FOTOTOOLSCHEMA.BENUTZER drop constraint FK_BENUTZER_GRUPPE;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN drop constraint FK_BESTELLUNGEN_BESTELLUNGEN_BILDER;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN drop constraint FK_BESTELLUNGEN_BENUTZER;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN drop constraint FK_BESTELLUNGEN_BILDER_AUSWAHL;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN_BILDER drop constraint FK_BESTELLUNGEN_BILDER_BENUTZER;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN_BILDER drop constraint FK_BESTELLUNGEN_BILDER_BESTELLUNGEN;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN_BILDER drop constraint FK_BESTELLUNGEN_BILDER;
alter table FOTOTOOLSCHEMA.BILD_BILDER_AUSWAHL drop constraint FK_BILD_BILDER_AUSWAHL_BILDER;
alter table FOTOTOOLSCHEMA.BILD_BILDER_AUSWAHL drop constraint FK_BILD_BILDER_AUSWAHL_BILDER_AUSWAHL;
alter table FOTOTOOLSCHEMA.BILDER_AUSWAHL drop constraint FK_BILDER_AUSWAHL_ABRECHNUNGEN;
alter table FOTOTOOLSCHEMA.BILDER_AUSWAHL_GRUPPE drop constraint FK_BILDER_AUSWAHL_GRUPPE_GRUPPEN;
alter table FOTOTOOLSCHEMA.BILDER_AUSWAHL_GRUPPE drop constraint FK_BILDER_AUSWAHL_GRUPPE_BILDER_AUSWAHL;
alter table FOTOTOOLSCHEMA.GRUPPEN_RECHTE drop constraint FK_GRUPPEN_RECHTE_RECHTE;
alter table FOTOTOOLSCHEMA.GRUPPEN_RECHTE drop constraint FK_GRUPPEN_RECHTE_GRUPPEN;
--TABLES
drop table FOTOTOOLSCHEMA.BENUTZER;
drop table FOTOTOOLSCHEMA.BESTELLUNGEN;
drop table FOTOTOOLSCHEMA.BESTELLUNGEN_BILDER;
drop table FOTOTOOLSCHEMA.ABRECHNUNGEN;
drop table FOTOTOOLSCHEMA.BILD_BILDER_AUSWAHL;
drop table FOTOTOOLSCHEMA.BILDER_AUSWAHL_GRUPPE;
drop table FOTOTOOLSCHEMA.BILDER_AUSWAHL;
drop table FOTOTOOLSCHEMA.BILDER;
drop table FOTOTOOLSCHEMA.GRUPPEN;
drop table FOTOTOOLSCHEMA.GRUPPEN_RECHTE;
drop table FOTOTOOLSCHEMA.RECHTE;
-- SCHEMA
DROP SCHEMA FOTOTOOLSCHEMA RESTRICT;

-- CREATE
-- SCHEMA
CREATE SCHEMA FOTOTOOLSCHEMA AUTHORIZATION root;
SET SCHEMA FOTOTOOLSCHEMA;
-- SEQUENCE
create sequence FOTOTOOLSCHEMA.hibernate_sequence start with 1 increment by 1;
-- BENUTZER
create table FOTOTOOLSCHEMA.BENUTZER (id bigint not null, crea timestamp, lmod timestamp, loginname varchar(255), name varchar(255), passwort varchar(255), vorname varchar(255), GRUPPE bigint, primary key (id));
-- BESTELLUNGEN
create table FOTOTOOLSCHEMA.BESTELLUNGEN (id bigint not null, crea timestamp, lmod timestamp, bezeichnung varchar(255), bestellungBilder_id bigint, BENUTZER bigint, BESTELLUNG bigint, primary key (id));
-- BESTELLUNGEN_BILDER
create table FOTOTOOLSCHEMA.BESTELLUNGEN_BILDER (id bigint not null, crea timestamp, lmod timestamp, anzahl integer, BESTELLER bigint not null, BESTELLUNG bigint not null, BILD bigint not null, primary key (id));
-- ABRECHNUGEN
create table FOTOTOOLSCHEMA.ABRECHNUNGEN (id bigint not null, crea timestamp, lmod timestamp, primary key (id));
-- BILD_BILDER_AUSWAHL
create table FOTOTOOLSCHEMA.BILD_BILDER_AUSWAHL (BILDER_AUSWAHL bigint not null, BILD bigint not null, bilder_KEY bigint not null, primary key (BILDER_AUSWAHL, bilder_KEY));
-- BILDER
create table FOTOTOOLSCHEMA.BILDER (id bigint not null, crea timestamp, lmod timestamp, dateiName varchar(255), dateiOrdner varchar(255), name varchar(255), primary key (id));
-- BILDER_AUSWAHL
create table FOTOTOOLSCHEMA.BILDER_AUSWAHL (id bigint not null, crea timestamp, lmod timestamp, gueltigBis date, name varchar(255), abrechnung_id bigint, primary key (id));
-- BILDER_AUSWAHL_GRUPPE
create table FOTOTOOLSCHEMA.BILDER_AUSWAHL_GRUPPE (BILDER_AUSWAHL bigint not null, GRUPPE bigint not null);
-- GRUPPEN
create table FOTOTOOLSCHEMA.GRUPPEN (id bigint not null, crea timestamp, lmod timestamp, beschreibung varchar(255), name varchar(255), primary key (id));
-- GRUPPE_RECHTE
create table FOTOTOOLSCHEMA.GRUPPEN_RECHTE (gruppe_fk bigint not null, rechte_fk bigint not null);
-- RECHTE
create table FOTOTOOLSCHEMA.RECHTE (id bigint not null, crea timestamp, lmod timestamp, beschreibung varchar(255), name varchar(255), primary key (id));

-- ADD CONSTRAINTS
alter table FOTOTOOLSCHEMA.BENUTZER add constraint FK_BENUTZER_GRUPPE foreign key (GRUPPE) references FOTOTOOLSCHEMA.GRUPPEN;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN add constraint FK_BESTELLUNGEN_BESTELLUNGEN_BILDER foreign key (bestellungBilder_id) references FOTOTOOLSCHEMA.BESTELLUNGEN_BILDER;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN add constraint FK_BESTELLUNGEN_BENUTZER foreign key (BENUTZER) references FOTOTOOLSCHEMA.BENUTZER;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN add constraint FK_BESTELLUNGEN_BILDER_AUSWAHL foreign key (BESTELLUNG) references FOTOTOOLSCHEMA.BILDER_AUSWAHL;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN_BILDER add constraint FK_BESTELLUNGEN_BILDER_BENUTZER foreign key (BESTELLER) references FOTOTOOLSCHEMA.BENUTZER;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN_BILDER add constraint FK_BESTELLUNGEN_BILDER_BESTELLUNGEN foreign key (BESTELLUNG) references FOTOTOOLSCHEMA.BESTELLUNGEN;
alter table FOTOTOOLSCHEMA.BESTELLUNGEN_BILDER add constraint FK_BESTELLUNGEN_BILDER foreign key (BILD) references FOTOTOOLSCHEMA.BILDER;
alter table FOTOTOOLSCHEMA.BILD_BILDER_AUSWAHL add constraint FK_BILD_BILDER_AUSWAHL_BILDER foreign key (BILD) references FOTOTOOLSCHEMA.BILDER;
alter table FOTOTOOLSCHEMA.BILD_BILDER_AUSWAHL add constraint FK_BILD_BILDER_AUSWAHL_BILDER_AUSWAHL foreign key (BILDER_AUSWAHL) references FOTOTOOLSCHEMA.BILDER_AUSWAHL;
alter table FOTOTOOLSCHEMA.BILDER_AUSWAHL add constraint FK_BILDER_AUSWAHL_ABRECHNUNGEN foreign key (abrechnung_id) references FOTOTOOLSCHEMA.ABRECHNUNGEN;
alter table FOTOTOOLSCHEMA.BILDER_AUSWAHL_GRUPPE add constraint UK_GRUPPEN_ID unique (GRUPPE);
alter table FOTOTOOLSCHEMA.BILDER_AUSWAHL_GRUPPE add constraint FK_BILDER_AUSWAHL_GRUPPE_GRUPPEN foreign key (GRUPPE) references FOTOTOOLSCHEMA.GRUPPEN;
alter table FOTOTOOLSCHEMA.BILDER_AUSWAHL_GRUPPE add constraint FK_BILDER_AUSWAHL_GRUPPE_BILDER_AUSWAHL foreign key (BILDER_AUSWAHL) references FOTOTOOLSCHEMA.BILDER_AUSWAHL;
alter table FOTOTOOLSCHEMA.GRUPPEN_RECHTE add constraint FK_GRUPPEN_RECHTE_RECHTE foreign key (rechte_fk) references FOTOTOOLSCHEMA.RECHTE;
alter table FOTOTOOLSCHEMA.GRUPPEN_RECHTE add constraint FK_GRUPPEN_RECHTE_GRUPPEN foreign key (gruppe_fk) references FOTOTOOLSCHEMA.GRUPPEN;