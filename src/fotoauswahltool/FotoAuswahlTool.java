/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool;

import fotoauswahltool.fxml.PaneNavigator;
import fotoauswahltool.service.IAdminController;
import fotoauswahltool.service.IController;
import hib.dao.DaoFactory;
import hib.model.Benutzer;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import manager.Authenticator;
import manager.DatabaseManager;
import utils.StringUtils;

/**
 *
 * @author simon
 */
public class FotoAuswahlTool extends Application {
    public static final URL PLACEHOLDER_IMG_URL = PaneNavigator.class.getResource("resources/placeholder.png");
    private Stage stage;
    final private Label label = new Label("");
    
//    SharedScene
    private Parent parentNode;
    
//    logged in user
    private Benutzer loggedInUser;
    
    private void mayBeShow() {
        if (loggedInUser != null) {
            String fxml = PaneNavigator.MAIN_USER;
            final boolean isAdmin = DaoFactory.getGruppeDao().isAdministrator(loggedInUser);
            if (isAdmin) {
                fxml = PaneNavigator.MAIN_ADMIN;
            }

            try {
                stage.setScene(createScene(loadPane(fxml)));
            }
            catch (Exception e) {
                System.err.println("Exception:" + e.getMessage());
            }

            if (isAdmin) {
                PaneNavigator.loadUserVista(PaneNavigator.VISTA_BENUTZER_VERWALTUNG);
                PaneNavigator.loadFotoVista(PaneNavigator.VISTA_FOTO_VERWALTUNG);
            }
            else {
//                PaneNavigator.loadVista(PaneNavigator.);
            }
            // Show the application if it has credentials and 
            // the application stage is ready
            if (stage != null) {
                Platform.runLater(() -> { 
                    stage.show(); 
                });
            }
        }
    }
    
    private Pane loadPane(final String fxml) throws IOException {
        final FXMLLoader loader = new FXMLLoader();
        
        Pane mainPane = null;
        try (InputStream in = getClass().getResourceAsStream(fxml);) {
            mainPane = (Pane) loader.load(in);
        }
        catch (IOException ioe) {
            System.err.println("Exception: " + ioe.getMessage());
        }
        
        final IController mainController = loader.getController();
        mainController.setApp(this);
        
        if (mainController instanceof IAdminController) {
            PaneNavigator.setMainController((IAdminController) mainController);
        }
        
        return mainPane;
    }
    
    private Scene createScene(Pane mainPane) {
        return(new Scene(mainPane));
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        
        stage.setTitle("Fotoauswahl Tool");
        stage.setScene(createScene(loadPane(PaneNavigator.LOGIN)));

        stage.show();
        // maybe show stage
//        mayBeShow();
//        
        stage.setOnCloseRequest((WindowEvent we) -> {
            DatabaseManager.close();
            System.out.println("WindowEvent: " + we.toString());
            System.out.println("Stage is closing");
            Platform.exit();
            System.exit(0);
        });
    }

//    @Override
    public void setCredential(String user, String password) {
        if (StringUtils.isBlank(user) && StringUtils.isBlank(password)) {
            loggedInUser = null;
            logOut();
        }
        else {
            loggedInUser = Authenticator.validate(user, password);
            label.setText("Hello " + user + "!");
            mayBeShow();
        }
    }
    
    private void logOut() {
        Platform.runLater(() -> { stage.hide(); });
        
        try {
            final Pane loginPane = loadPane("fxml/login.fxml");
            stage.setScene(createScene(loginPane));
        }
        catch (IOException ioe) {
            System.err.println("Exception: " + ioe.getMessage());
        }
        
        Platform.runLater(() -> {
            stage.show();
        });
    }

//    @Override
    public Parent getParentNode() {
        return parentNode;
    }

    public Benutzer getLoggedInUser() {
        return loggedInUser;
    }

    public Stage getStage() {
        return stage;
    }

}
