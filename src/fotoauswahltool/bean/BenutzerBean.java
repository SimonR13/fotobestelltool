/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.bean;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author simon
 */
public class BenutzerBean {
    
    private StringProperty vorname;
    private StringProperty name;
    private StringProperty loginname;
    private LongProperty id;
    private StringProperty isAdmin;
    private BooleanProperty resetPasswort;

    public StringProperty vornameProperty() { 
        if (vorname == null) {
            vorname = new SimpleStringProperty(this, "vorname");
        }
        return vorname; 
    }
    
    public StringProperty nameProperty() {
        if (name == null) {
            name = new SimpleStringProperty(this, "name");
        }
        return name;
    }
    
    public StringProperty loginnameProperty() {
        if (loginname == null) {
            loginname = new SimpleStringProperty(this, "loginname");
        }
        return loginname;
    }
    
    public StringProperty isAdminProperty() {
        if (isAdmin == null) {
            isAdmin = new SimpleStringProperty(this, "isAdmin");
        }
        return isAdmin;
    }
    
    public LongProperty idProperty() {
        if (id == null) {
            id = new SimpleLongProperty(this, "id");
        }
        return id;
    }
    
    public BooleanProperty resetPasswortProperty() {
        if (resetPasswort == null) {
            resetPasswort = new SimpleBooleanProperty(this, "resetPasswort");
        }
        return resetPasswort;
    }

    public String getVorname() {
        return vornameProperty().get();
    }

    public void setVorname(String vorname) {
        vornameProperty().set(vorname);
    }

    public String getName() {
        return nameProperty().get();
    }

    public void setName(String name) {
        nameProperty().set(name);
    }

    public String getLoginname() {
        return loginnameProperty().get();
    }

    public void setLoginname(String loginname) {
        loginnameProperty().set(loginname);
    }

    public Long getId() {
        return idProperty().get();
    }

    public void setId(Long id) {
        idProperty().set(id);
    }

    public String getIsAdmin() {
        return isAdminProperty().get();
    }

    public void setIsAdmin(boolean isAdmin) {
        isAdminProperty().set(Boolean.toString(isAdmin));
    }

    public Boolean getResetPasswort() {
        return resetPasswortProperty().get();
    }

    public void setResetPasswort(Boolean resetPasswort) {
        resetPasswortProperty().set(true);
    }
    
}
