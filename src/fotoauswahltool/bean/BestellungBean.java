/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.bean;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author simon
 */
public class BestellungBean {
    
    private Long id;
    private String bezeichnung;
    private BenutzerBean ersteller;
    private List<BildBestellungBean> bildBestellungen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public BenutzerBean getErsteller() {
        return ersteller;
    }

    public void setErsteller(BenutzerBean ersteller) {
        this.ersteller = ersteller;
    }

    public List<BildBestellungBean> getBildBestellungen() {
        if (bildBestellungen == null) 
            setBildBestellungen(new ArrayList<>());
        
        return bildBestellungen;
    }

    public void setBildBestellungen(List<BildBestellungBean> bildBestellungen) {
        this.bildBestellungen = bildBestellungen;
    }
    
    
}
