/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.bean;

import java.io.File;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author simon
 */
public class BildBean {
    
    private StringProperty name;
    private StringProperty pfad;
    private StringProperty dateiName;
    
    private File fileReference;
    
    private StringProperty dateiNameProperty() {
        if (dateiName == null) {
            dateiName = new SimpleStringProperty(this, "dateiName");
        }
        return dateiName;
    }

    private StringProperty pfadProperty() {
        if (pfad == null) {
            pfad = new SimpleStringProperty(this, "pfad");
        }
        return pfad;
    }
    
    private StringProperty nameProperty() {
        if (name == null) {
            name = new SimpleStringProperty(this, "name");
        }
        return name;
    }
    
    public String getName() {
        return(nameProperty().get());
    }

    public void setName(String name) {
        nameProperty().set(name);
    }

    public String getPfad() {
        return(pfadProperty().get());
    }

    public void setPfad(String pfad) {
        pfadProperty().set(pfad);
    }
    
    public String getDateiName() {
        return(dateiNameProperty().get());
    }
    
    public void setDateiName(String dateiName) {
        dateiNameProperty().set(dateiName);
    }

    public File getFileReference() {
        return fileReference;
    }

    public void setFileReference(File fileReference) {
        this.fileReference = fileReference;
    }
}
