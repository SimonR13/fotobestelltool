/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.bean;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author simon
 */
public class BildBestellungBean {
    
    private Long id;
    private List<BildBean> bilder;
    private Integer anzahl;
    private BenutzerBean besteller;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<BildBean> getBilder() {
        if (bilder == null) 
            setBilder(new ArrayList<>());
        
        return bilder;
    }

    public void setBilder(List<BildBean> bilder) {
        this.bilder = bilder;
    }

    public Integer getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(Integer anzahl) {
        this.anzahl = anzahl;
    }

    public BenutzerBean getBesteller() {
        return besteller;
    }

    public void setBesteller(BenutzerBean besteller) {
        this.besteller = besteller;
    }
    
    
    
}
