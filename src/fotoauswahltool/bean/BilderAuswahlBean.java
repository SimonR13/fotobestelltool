/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.bean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author simon
 */
public class BilderAuswahlBean {
    
    private Long id;
    private String name;
    private LocalDate gueltigBis;
    
    private List<BildBean> bilder;
    private List<BreiteHoehePreisBean> bildGroessePreis;
    private List<BestellungBean> bestellungen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getGueltigBis() {
        return gueltigBis;
    }

    public void setGueltigBis(LocalDate gueltigBis) {
        this.gueltigBis = gueltigBis;
    }

    public List<BildBean> getBilder() {
        if (bilder == null) 
            setBilder(new ArrayList<>());
        
        return bilder;
    }

    public void setBilder(List<BildBean> bilder) {
        this.bilder = bilder;
    }

    public List<BreiteHoehePreisBean> getBildGroessePreis() {
        if (bildGroessePreis == null) 
            setBildGroessePreis(new ArrayList<>());

        return bildGroessePreis;
    }

    public void setBildGroessePreis(List<BreiteHoehePreisBean> bildGroessePreis) {
        this.bildGroessePreis = bildGroessePreis;
    }

    public List<BestellungBean> getBestellungen() {
        if (bestellungen == null) 
            setBestellungen(new ArrayList<>());
        
        return bestellungen;
    }

    public void setBestellungen(List<BestellungBean> bestellungen) {
        this.bestellungen = bestellungen;
    }
    
}
