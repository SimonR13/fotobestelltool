/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author simon
 */
public class BreiteHoehePreisBean {
    
    private Long id = -1L;
    private SimpleIntegerProperty breite = new SimpleIntegerProperty();
    private SimpleIntegerProperty hoehe = new SimpleIntegerProperty();
    
    private SimpleDoubleProperty preis = new SimpleDoubleProperty();

    public BreiteHoehePreisBean() {
        this(9, 6, 0.14D);
    }

    public BreiteHoehePreisBean(Integer breit, Integer hoehe, Double preis) {
        this.breite.set(breit);
        this.hoehe.set(hoehe);
        this.preis.set(preis);
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBreite() {
        return breite.get();
    }

    public void setBreite(Integer breite) {
        this.breite.set(breite);
    }

    public Integer getHoehe() {
        return hoehe.get();
    }

    public void setHoehe(Integer hoehe) {
        this.hoehe.set(hoehe);
    }

    public Double getPreis() {
        return preis.get();
    }

    public void setPreis(Double preis) {
        this.preis.set(preis);
    }
    
}
