/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.controller;

import fotoauswahltool.FotoAuswahlTool;
import fotoauswahltool.service.IAdminController;
import hib.model.Benutzer;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author simon
 */
public class AdministratorController implements IAdminController {
    private FotoAuswahlTool application;
    private ObservableList<Benutzer> userList;
    
    @FXML
    private AnchorPane userAnchorPane;
    @FXML
    private AnchorPane fotoAnchorPane;
    
    @FXML
    public void handleSignOut() {
        application.setCredential(null, null);
    }
    
    @Override
    public void setApp(final Application app) {
        this.application = (FotoAuswahlTool) app;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO
    }

    public AnchorPane getUserAnchorPane() {
        return userAnchorPane;
    }

    public void setUserAnchorPane(AnchorPane userAnchorPane) {
        this.userAnchorPane = userAnchorPane;
    }

    public AnchorPane getFotoAnchorPane() {
        return fotoAnchorPane;
    }

    public void setFotoAnchorPane(AnchorPane fotoAnchorPane) {
        this.fotoAnchorPane = fotoAnchorPane;
    }

    @Override
    public void setUserVista(Node node) {
        if (userAnchorPane.getChildren().isEmpty()) {
            userAnchorPane.getChildren().setAll(node);
        }
    }

    @Override
    public void setFotoVista(Node node) {
        if (fotoAnchorPane.getChildren().isEmpty()) {
            fotoAnchorPane.getChildren().setAll(node);
        }
    }
    
}
