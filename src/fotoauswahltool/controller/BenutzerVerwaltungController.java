/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.controller;

import fotoauswahltool.FotoAuswahlTool;
import fotoauswahltool.bean.BenutzerBean;
import fotoauswahltool.service.IController;
import fotoauswahltool.service.factory.HibernateBeanFactory;
import hib.dao.DaoFactory;
import hib.model.Benutzer;
import hib.model.dao.BenutzerDao;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.SequentialTransition;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;
import utils.StringUtils;

/**
 *
 * @author simon
 */
public class BenutzerVerwaltungController implements IController {
    private FotoAuswahlTool application;
    private ObservableList<BenutzerBean> userList;
    private BenutzerBean selectedBenutzer;
            
    @FXML
    private TableView<BenutzerBean> tblUser;
    @FXML
    private TextField txtVorname;
    @FXML
    private TextField txtName;
    @FXML
    private TextField txtLoginname;
    @FXML
    private CheckBox chkIsAdmin;
    @FXML
    private CheckBox chkResetPasswort;
    @FXML
    private Label lblError;

    @FXML
    public void handleVerwerfen() {
        verwerfeAuswahl();
    }
    
    @FXML
    public void handleLoeschen() {
        loescheAuswahl();
    }
    
    @FXML
    public void handleSpeichern() {
        speichereBenutzer();
    }
    
    public TableView<BenutzerBean> getTblUser() {
        return tblUser;
    }

    public void setTblUser(TableView<BenutzerBean> tblUser) {
        this.tblUser = tblUser;
    }
    
    @Override
    public void setApp(Application app) {
        this.application = (FotoAuswahlTool) app;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initList();
    }
    
    private void initList() {
        final ObservableList<Benutzer> benutzerList = DaoFactory.getBenutzerDao().getBenutzerList();
        
        userList = HibernateBeanFactory.getBenutzerBeanList(benutzerList);
        getTblUser().setItems(userList);
        
        final ObservableList<TableColumn<BenutzerBean, ?>> columns = getTblUser().getColumns();

        final TableColumn<BenutzerBean, Long> idClmn = 
                (TableColumn<BenutzerBean, Long>) columns.get(0);
        idClmn.setCellValueFactory(new PropertyValueFactory("id"));
        
        final TableColumn<BenutzerBean, String> vornameClmn = 
                (TableColumn<BenutzerBean, String>) columns.get(1);
        vornameClmn.setCellValueFactory(new PropertyValueFactory("vorname"));
        
        final TableColumn<BenutzerBean, String> nameClmn = 
                (TableColumn<BenutzerBean, String>) columns.get(2);
        nameClmn.setCellValueFactory(new PropertyValueFactory("name"));
        
        final TableColumn<BenutzerBean, String> loginnameClmn = 
                (TableColumn<BenutzerBean, String>) columns.get(3);
        loginnameClmn.setCellValueFactory(new PropertyValueFactory("loginname"));
        
        final TableColumn<BenutzerBean, String> isAdminClmn = 
                (TableColumn<BenutzerBean, String>) columns.get(4);
        isAdminClmn.setCellValueFactory(new PropertyValueFactory("isAdmin"));
        
        getTblUser().getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        getTblUser().getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> { 
                selectedBenutzer = newSelection;
                if (newSelection != null) {
                    txtLoginname.setText(newSelection.getLoginname());
                    txtName.setText(newSelection.getName());
                    txtVorname.setText(newSelection.getVorname());
                    chkIsAdmin.setSelected(Boolean.getBoolean(newSelection.getIsAdmin()));
                    chkResetPasswort.setSelected(Boolean.FALSE);
                }
                if (oldSelection != null && newSelection == null) {
                    verwerfeAuswahl();
                }
            }
        );
    }
    
    private void verwerfeAuswahl() {
        txtLoginname.setText("");
        txtVorname.setText("");
        txtName.setText("");
        chkIsAdmin.setSelected(Boolean.FALSE);
        chkResetPasswort.setSelected(Boolean.TRUE);
        selectedBenutzer = null;
    }
    
    private void loescheAuswahl() {
        final BenutzerDao benutzerDao = DaoFactory.getBenutzerDao();
        
        final Benutzer selectedUser = 
                benutzerDao.findById(selectedBenutzer.getId());
        
        tblUser.getItems().remove(selectedBenutzer);
        
        benutzerDao.delete(selectedUser);
        verwerfeAuswahl();
    }

    private void speichereBenutzer() {
        final String newLoginname = txtLoginname.getText();
        if (selectedBenutzer == null && StringUtils.isNotBlank(newLoginname)
                && (DaoFactory.getBenutzerDao().getBenutzerByLoginname(newLoginname) == null)) {
            speichereNeuenBenutzer();
        }
        else {
            aktualisiereBenutzer();
        }
    }
    
    private void speichereNeuenBenutzer() {
        final String newLoginname = txtLoginname.getText();
        final String newName = txtName.getText();
        final String newVorname = txtVorname.getText();
        final boolean newIsAdmin = chkIsAdmin.isSelected();
        final boolean newResetPw = chkResetPasswort.isSelected();
        
        if (StringUtils.isNotBlank(newName) && 
                StringUtils.isNotBlank(newLoginname) &&
                StringUtils.isNotBlank(newVorname)) {
            
            final Benutzer newBenutzer = new Benutzer();
            newBenutzer.setLoginname(newLoginname);
            newBenutzer.setName(newName);
            newBenutzer.setVorname(newVorname);
            if (newIsAdmin) {
                newBenutzer.setGruppe(DaoFactory.getGruppeDao().getAdminGruppe());
            }
            else {
                newBenutzer.setGruppe(DaoFactory.getGruppeDao().getBenutzerGruppe());
            }
            newBenutzer.setResetPasswort(newResetPw);
            newBenutzer.setPasswort("@" 
                    + newVorname.toLowerCase().charAt(0) 
                    + newName.toLowerCase().charAt(0) 
                    + "1!");
            
            DaoFactory.getBenutzerDao().save(newBenutzer);
            final BenutzerBean newBean = HibernateBeanFactory.getBenutzerBeanFromBo(newBenutzer);
            getTblUser().getItems().add(newBean);
            
            createInformationBar(newBenutzer);
            
            verwerfeAuswahl();
        }
    }
    
    private void createInformationBar(Benutzer newBenutzer) {
        if (newBenutzer != null) {
            final String text = "Der Benutzer mit Loginname '" + newBenutzer.getLoginname() + "'"
                    + " wurde mit dem Standardpasswort : '" + newBenutzer.getPasswort();
            
            createInformationBar(text);
        }
    }
    
    private void createInformationBar(final String text) {
        lblError.setText(text);

        final FadeTransition fade = new FadeTransition(Duration.seconds(20), lblError);
        fade.setFromValue(1);
        fade.setToValue(0);
        
        final SequentialTransition seqTrans = new SequentialTransition(lblError, fade);
        
        lblError.setVisible(Boolean.TRUE);
        seqTrans.play();
    }
    
    private void aktualisiereBenutzer() {
        final String newName = txtName.getText();
        final String newVorname = txtVorname.getText();
        final boolean newIsAdmin = chkIsAdmin.isSelected();
        final boolean newResetPw = chkResetPasswort.isSelected();
        
        final BenutzerDao benutzerDao = DaoFactory.getBenutzerDao();
        final Benutzer existingUser = benutzerDao.findById(selectedBenutzer.getId());
        existingUser.setName(newName);
        existingUser.setVorname(newVorname);
        existingUser.setResetPasswort(newResetPw);
        if (newIsAdmin) {
            existingUser.setGruppe(DaoFactory.getGruppeDao().getAdminGruppe());
        }
        else {
            existingUser.setGruppe(DaoFactory.getGruppeDao().getBenutzerGruppe());
        }
        
        benutzerDao.update(existingUser);
        selectedBenutzer.setName(newName);
        selectedBenutzer.setVorname(newVorname);
        selectedBenutzer.setResetPasswort(newResetPw);
        verwerfeAuswahl();
    }

}
