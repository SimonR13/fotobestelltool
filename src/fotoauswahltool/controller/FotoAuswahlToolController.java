/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.controller;

import fotoauswahltool.FotoAuswahlTool;
import fotoauswahltool.service.IController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import utils.StringUtils;

/**
 *
 * @author simon
 */
public class FotoAuswahlToolController implements IController {
    private FotoAuswahlTool application;
    private final FileChooser fileChooser = new FileChooser();

    @FXML
    private Label lblHallo;
    
    @FXML
    private ImageView imgView;
    
    @FXML
    public void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        lblHallo.setText("Hello World!");
    }
    
    @FXML
    public void handleSpeichernLogout(ActionEvent event) {
        application.setCredential(null, null);
    }
    
    @FXML
    public void handleAuswahlDrucken(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        getImgView().setImage(new Image(FotoAuswahlTool.PLACEHOLDER_IMG_URL.toString(), Boolean.TRUE));
    }    

    public Label getLabel() {
        return lblHallo;
    }

    @Override
    public void setApp(Application app) {
        this.application = (FotoAuswahlTool) app;
        
        final String welcomeName = (null != this.application.getLoggedInUser()) ? this.application.getLoggedInUser().getKonkreterName() : null;
        final String welcomeString = (StringUtils.isBlank(welcomeName)) ? "Willkommen!" : "Willkommen " + welcomeName;
        getLabel().setText(welcomeString);
    }

    public ImageView getImgView() {
        return imgView;
    }

    public void setImgView(ImageView imgView) {
        this.imgView = imgView;
    }

    private void displayPicture(final URL fileUrl) {
        getImgView().setImage(new Image(fileUrl.toString(), Boolean.TRUE));
    }

}
