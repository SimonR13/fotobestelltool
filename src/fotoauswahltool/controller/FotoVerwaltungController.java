/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.controller;

import fotoauswahltool.FotoAuswahlTool;
import fotoauswahltool.bean.BildBean;
import fotoauswahltool.bean.BilderAuswahlBean;
import fotoauswahltool.bean.BreiteHoehePreisBean;
import fotoauswahltool.fxml.PaneNavigator;
import fotoauswahltool.service.IController;
import fotoauswahltool.service.factory.HibernateBeanFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import manager.PersistenceManager;
import utils.StringUtils;

/**
 *
 * @author simon
 */
public class FotoVerwaltungController implements IController {
    private static FotoAuswahlTool application;
    private double initX;
    private double initY;
    private final FileChooser fileChooser = new FileChooser();
    
    private static final List<BildBean> BILDER_LISTE = new ArrayList<>();
    private static final ObservableList<BreiteHoehePreisBean> BREITE_HOEHE_PREIS_LISTE =  FXCollections.observableArrayList();
    private static LocalDate gueltigVon = LocalDate.now(), gueltigBis = LocalDate.of(2016, 04, 30);
    private static String auswahlBez = "Testbezeichnung";
    
    private static Stage stage = null;
    
    @FXML
    private FlowPane flowPaneAuswahlDateien;
    
    
    @FXML
    private TextField auswahlBezeichnung;
    @FXML
    private DatePicker dateGueltigkeitVon;
    @FXML
    private DatePicker dateGueltigkeitBis;
    
    @FXML
    private TableView tblBreiteHoehePreis;
    
    @FXML
    public void handleAuswahlDateien(ActionEvent event) {
        fileChooser.setTitle("Bilder auswählen");
        fileChooser.getExtensionFilters().clear();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        final List<File> fileList = fileChooser.showOpenMultipleDialog(new Stage());
        handleSelectedFiles(fileList);
        renderAndDisplayThumbnails();
    }
    
    @FXML
    public void handleNeueAuswahl(ActionEvent event) {
        // neue stage mit auswahlDateien.fxml layout
        final Scene dateiAuswahlScene = createScene("admin/fragmente/fotoauswahl/auswahlDateien.fxml", 400D, 600D);
        getStage().setScene(dateiAuswahlScene);
        getStage().show();
    }

    @FXML
    public void handleAuswahlDateienAbbruch(ActionEvent event) {
        clearInput();
        getStage().close();
    }
    
    @FXML
    public void handleAuswahlDateienWeiter(ActionEvent event) {
        if (!BILDER_LISTE.isEmpty()) {
            getStage().close();
            final Scene auswahlInfosScene = createScene("admin/fragmente/fotoauswahl/auswahlInfos.fxml", 400D, 600D);
            getStage().setScene(auswahlInfosScene);
            getStage().show();
        }
    }
    
    @FXML 
    public void handleAuswahlInfosWeiter(ActionEvent event) {
        if (handleInfos()) {
            getStage().close();
            final Scene auswahlInfosScene = createScene("admin/fragmente/fotoauswahl/auswahlGroessen.fxml", 400D, 600D);
            getStage().setScene(auswahlInfosScene);
            getStage().show();
        }
    }
    
    @FXML
    public void handleAuswahlInfosZurueck(ActionEvent event) {
        getStage().close();
        handleInfos();
        handleNeueAuswahl(event);
    }
    
    @FXML
    public void handleNeueAuswahlSpeichern(ActionEvent event) {
        if (getTblBreiteHoehePreis() != null && !(getTblBreiteHoehePreis().getItems().isEmpty())){
            BREITE_HOEHE_PREIS_LISTE.addAll(getTblBreiteHoehePreis().getItems());
        }
        final BilderAuswahlBean bilderAuswahl = new BilderAuswahlBean();
        bilderAuswahl.setBilder(BILDER_LISTE);
        bilderAuswahl.setGueltigBis(gueltigBis);
        bilderAuswahl.setName(auswahlBez);
        bilderAuswahl.setBildGroessePreis(BREITE_HOEHE_PREIS_LISTE);
        
        if (PersistenceManager.persistNeueAuswahl(bilderAuswahl, BREITE_HOEHE_PREIS_LISTE)) {
            clearInput();
            getStage().close();
        }
        // TODO else error
    }
    
    @FXML
    public void handleAuswahlGroesseZurueck(ActionEvent event) {
        getStage().close();
        handleAuswahlDateienWeiter(event);
    }
    
    @FXML
    public void handleBearbeiten(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @FXML
    public void handleEntfernen(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void setApp(Application app) {
        this.application = (FotoAuswahlTool) app;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initStages();
    }

    public FlowPane getFlowPaneAuswahlDateien() {
        return flowPaneAuswahlDateien;
    }

    public void setFlowPaneAuswahlDateien(FlowPane flowPaneAuswahlDateien) {
        this.flowPaneAuswahlDateien = flowPaneAuswahlDateien;
    }

    private void handleSelectedFiles(List<File> fileList) {
        BILDER_LISTE.clear();
        fileList.stream().filter((file) -> (file.canRead() && file.isFile())).map((file) -> {
            final BildBean bean = HibernateBeanFactory.createBildBean(file.getName(), file.getPath());
            bean.setFileReference(file);
            return bean;
        }).forEach((bean) -> {
            BILDER_LISTE.add(bean);
        });
        
    }
    
    private Scene createScene(String fxml, double height, double width) {
        final FXMLLoader loader = new FXMLLoader();
        
        Pane mainPane = null;
        try (InputStream in = PaneNavigator.class.getResourceAsStream(fxml);) {
            mainPane = (Pane) loader.load(in);
        }
        catch (IOException ioe) {
            System.err.println("Exception: " + ioe.getMessage());
        }
        
        final Scene newScene = new Scene(mainPane, width, height);
        newScene.getRoot().setOnMousePressed((MouseEvent rootMouseEvent) -> {
            initX = rootMouseEvent.getScreenX() - getStage().getX();
            initY = rootMouseEvent.getScreenY() - getStage().getY();
        });
        newScene.getRoot().setOnMouseDragged((MouseEvent rootMouseEvent) -> {
            getStage().setX(rootMouseEvent.getScreenX() - initX);
            getStage().setY(rootMouseEvent.getScreenY() - initY);
        });
        
        return newScene;
    }

    private ImageView getImageView(String pfad) {
        return(new ImageView(pfad));
    }

    public TextField getAuswahlBezeichnung() {
        return auswahlBezeichnung;
    }

    public void setAuswahlBezeichnung(TextField auswahlBezeichnung) {
        this.auswahlBezeichnung = auswahlBezeichnung;
    }

    public DatePicker getDateGueltigkeitVon() {
        return dateGueltigkeitVon;
    }

    public void setDateGueltigkeitVon(DatePicker dateGueltigkeitVon) {
        this.dateGueltigkeitVon = dateGueltigkeitVon;
    }

    public DatePicker getDateGueltigkeitBis() {
        return dateGueltigkeitBis;
    }

    public void setDateGueltigkeitBis(DatePicker dateGueltigkeitBis) {
        this.dateGueltigkeitBis = dateGueltigkeitBis;
    }

    public Stage getStage() {
        if (stage == null) {
            stage = new Stage(StageStyle.TRANSPARENT);
            stage.centerOnScreen();
            stage.setAlwaysOnTop(true);
            stage.initModality(Modality.APPLICATION_MODAL);
        }
        return stage;
    }

    public TableView getTblBreiteHoehePreis() {
        return tblBreiteHoehePreis;
    }

    public void setTblBreiteHoehePreis(TableView tblBreiteHoehePreis) {
        this.tblBreiteHoehePreis = tblBreiteHoehePreis;
    }

    public static FotoAuswahlTool getApplication() {
        return application;
    }

    private void clearInput() {
        BILDER_LISTE.clear();
        BREITE_HOEHE_PREIS_LISTE.clear();
        gueltigVon = LocalDate.now();
        gueltigBis = LocalDate.of(gueltigVon.getYear(), gueltigVon.getMonth().getValue() + 1, gueltigVon.getDayOfMonth());
        auswahlBez = null;
    }
    
    private void renderAndDisplayThumbnails() {
        if (BILDER_LISTE.isEmpty()) {
            if (flowPaneAuswahlDateien != null && flowPaneAuswahlDateien.getChildren().isEmpty()) {
                final Image placeHolderImg = new Image(FotoAuswahlTool.PLACEHOLDER_IMG_URL.toString());
                final ImageView imgView = new ImageView(placeHolderImg);
                imgView.setPreserveRatio(true);
                imgView.setFitHeight(200 - 10);
                flowPaneAuswahlDateien.getChildren().add(imgView);
            }
        }
        else if (flowPaneAuswahlDateien != null) {
            flowPaneAuswahlDateien.getChildren().clear();
            flowPaneAuswahlDateien.setHgap(5D);
            flowPaneAuswahlDateien.setVgap(5D);
            
            final int bilderZeile = (BILDER_LISTE.size() > 5) ? 5 : BILDER_LISTE.size(),
                    anzSpalten = (int) (BILDER_LISTE.size() / bilderZeile) + 1;
            int imgWidth = (int) flowPaneAuswahlDateien.getWidth() / bilderZeile - 5 ,
                    imgHeight = (int) flowPaneAuswahlDateien.getHeight() / anzSpalten - 5;
            imgWidth = (imgWidth < 1 || imgWidth > 110) ? 110 : imgWidth;
            imgHeight = (imgWidth < 1 || imgHeight > 110) ? 110 : imgHeight;
            try {
                for (BildBean bean : BILDER_LISTE) {
                    final ImageView imgView = getImageView(bean.getFileReference().toURI().toURL().toString());
                    imgView.setPreserveRatio(true);
                    imgView.setFitWidth(imgWidth);
                    imgView.setFitHeight(imgHeight);
                    flowPaneAuswahlDateien.getChildren().add(imgView);
                }
            }
            catch (MalformedURLException mue) {
                System.err.println("Exception" + mue.getMessage());
            }
        }
    }

    private void initFirstBreiteHoehePreisRow() {
        if (BREITE_HOEHE_PREIS_LISTE.isEmpty()) {    
            if (getTblBreiteHoehePreis() != null) {
                getTblBreiteHoehePreis().getColumns().clear();
                getTblBreiteHoehePreis().setEditable(true);
                getTblBreiteHoehePreis().setOnMouseClicked((MouseEvent event) -> {
                    if (event.getClickCount() == 2) {
                        final BreiteHoehePreisBean rowData = new BreiteHoehePreisBean(0, 0, 0.0D);
                        getTblBreiteHoehePreis().getItems().add(rowData);
                        getTblBreiteHoehePreis().refresh();
                    }
                });
                
                TableColumn breiteClmn = new TableColumn("Breite");
                breiteClmn.setMinWidth(75);
                breiteClmn.setCellValueFactory(new PropertyValueFactory<>("breite"));
                breiteClmn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
                breiteClmn.setOnEditCommit(
                    new EventHandler<CellEditEvent<BreiteHoehePreisBean, Integer>>() {
                        @Override
                        public void handle(CellEditEvent<BreiteHoehePreisBean, Integer> t) {
                            ((BreiteHoehePreisBean) t.getTableView().getItems().get(
                                    t.getTablePosition().getRow())).setBreite(t.getNewValue());
                        }
                    }
                );
                
                TableColumn hoeheClmn = new TableColumn("Höhe");
                hoeheClmn.setMinWidth(75);
                hoeheClmn.setCellValueFactory(new PropertyValueFactory<>("hoehe"));
                hoeheClmn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
                hoeheClmn.setOnEditCommit(
                    new EventHandler<CellEditEvent<BreiteHoehePreisBean, Integer>>() {
                        @Override
                        public void handle(CellEditEvent<BreiteHoehePreisBean, Integer> t) {
                            ((BreiteHoehePreisBean) t.getTableView().getItems().get(
                                    t.getTablePosition().getRow())
                                    ).setHoehe(t.getNewValue());
                        }
                    }
                );
                
                TableColumn preisClmn = new TableColumn("Preis");
                preisClmn.setMinWidth(75);
                preisClmn.setCellValueFactory(new PropertyValueFactory<>("preis"));
                preisClmn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
                preisClmn.setOnEditCommit(
                    new EventHandler<CellEditEvent<BreiteHoehePreisBean, Double>>() {
                        @Override
                        public void handle(CellEditEvent<BreiteHoehePreisBean, Double> t) {
                            ((BreiteHoehePreisBean) t.getTableView().getItems().get(
                                    t.getTablePosition().getRow())
                                    ).setPreis(t.getNewValue());
                        }
                    }
                );
                
                getTblBreiteHoehePreis().setItems(FXCollections.observableArrayList(new BreiteHoehePreisBean()));
                getTblBreiteHoehePreis().getColumns().addAll(breiteClmn,hoeheClmn,preisClmn);
                getTblBreiteHoehePreis().refresh();
            }
        }
        else if (getTblBreiteHoehePreis() != null) {
            getTblBreiteHoehePreis().setItems(BREITE_HOEHE_PREIS_LISTE);
            getTblBreiteHoehePreis().refresh();
        }
    }

    private void initAuswahlInfos() {
        if (getAuswahlBezeichnung() != null && StringUtils.isNotBlank(auswahlBez)) {
            getAuswahlBezeichnung().setText(auswahlBez);
        }
        if (getDateGueltigkeitBis() != null && gueltigBis != null) {
            getDateGueltigkeitBis().setValue(gueltigBis);
        }
        if (getDateGueltigkeitVon() != null && gueltigVon != null) {
            getDateGueltigkeitVon().setValue(gueltigVon);
        }
    }

    private void initStages() {
        renderAndDisplayThumbnails();
        initAuswahlInfos();
        initFirstBreiteHoehePreisRow();
    }

    private boolean handleInfos() {
        boolean retVal = true;
        if (getAuswahlBezeichnung() != null && StringUtils.isNotBlank(getAuswahlBezeichnung().getText())) {
            auswahlBez = getAuswahlBezeichnung().getText();
        }
        else {
            retVal = false;
        }
        if (getDateGueltigkeitBis() != null && getDateGueltigkeitBis().getValue() != null) {
            gueltigBis = getDateGueltigkeitBis().getValue();
        }
        else {
            retVal = false;
        }
        if (getDateGueltigkeitVon() != null && getDateGueltigkeitVon().getValue() != null) {
            gueltigVon = getDateGueltigkeitVon().getValue();
        }
        else {
            retVal = false;
        }
        return retVal;
    }
    
}
