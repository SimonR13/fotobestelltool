/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.controller;

import fotoauswahltool.FotoAuswahlTool;
import fotoauswahltool.service.IController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import manager.DatabaseManager;

/**
 *
 * @author simon
 */
public class LoginController implements IController {
    private FotoAuswahlTool application;

    @FXML
    private TextField userId;
    
    @FXML
    private PasswordField password;
    
    @FXML
    private Label errorMessage;
    
    @FXML
    private ProgressBar progressBar;
    
    @FXML
    private Button loginButton;
    
    @FXML 
    private Button closeButton;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        errorMessage.setText("");
        userId.setPromptText("Benutzername");
        password.setPromptText("Passwort");
    }
    
    @FXML
    public void handleLogin() {
        try {
            application.setCredential(userId.getText(), password.getText());
        }
        catch (Exception e) {
            errorMessage.setVisible(Boolean.TRUE);
            errorMessage.setText("Exception: " + e.getMessage());
        }
    }
    
    @FXML
    public void handleClose() {
        // get a handle to the stage
        Stage stage = (Stage) closeButton.getScene().getWindow();
        // do what you have to do
        Platform.runLater(() -> {
            DatabaseManager.close();
            stage.close();
            Platform.exit();
            System.exit(0);
        });
    }
        
    public void handlePasswordBlur() {
        if (userId != null && password != null) {
            final String userStr = userId.getText();
            final String passwordStr = password.getText();
            
            if (userStr != null && userStr.length() > 1
                    && passwordStr != null & passwordStr.length() > 1) {
                loginButton.setDisable(Boolean.FALSE);
            }
            else {
                loginButton.setDisable(Boolean.FALSE);
            }
        }
    }
    
    public void handleKeyTyped() {
        if ((userId.getText() != null && userId.getText().length() > 1)
                && (password.getText() != null && password.getText().length() > 3)) {
            loginButton.setDisable(Boolean.FALSE);
        }
        else {
            loginButton.setDisable(Boolean.TRUE);
        }
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    @Override
    public void setApp(Application app) {
        this.application = (FotoAuswahlTool) app;
    }

}
