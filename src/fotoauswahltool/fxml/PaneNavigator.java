/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.fxml;

import fotoauswahltool.service.IAdminController;
import java.io.IOException;
import java.net.URL;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

/**
 *
 * @author simon
 */
public class PaneNavigator {
    /**
     * Convenience constants for fxml layouts managed by the navigator.
     */
    public static final String LOGIN    = "fxml/login.fxml";
    public static final String MAIN_USER    = "fxml/user/main_user.fxml";
    
    public static final String MAIN_ADMIN    = "fxml/admin/main.fxml";
    public static final String VISTA_BENUTZER_VERWALTUNG = "admin/fragmente/benutzerverwaltung.fxml";
    public static final String VISTA_FOTO_VERWALTUNG = "admin/fragmente/fotoverwaltung.fxml";

    /** The main application layout controller. */
    private static IAdminController mainController;

    /**
     * Stores the main controller for later use in navigation tasks.
     *
     * @param controller the main application layout controller.
     */
    public static void setMainController(IAdminController controller) {
        mainController = controller;
    }
    
    /**
     * Loads the vista specified by the fxml file into the
     * vistaHolder pane of the main application layout.
     *
     * Previously loaded vista for the same fxml file are not cached.
     * The fxml is loaded anew and a new vista node hierarchy generated
     * every time this method is invoked.
     *
     * A more sophisticated load function could potentially add some
     * enhancements or optimizations, for example:
     *   cache FXMLLoaders
     *   cache loaded vista nodes, so they can be recalled or reused
     *   allow a user to specify vista node reuse or new creation
     *   allow back and forward history like a browser
     *
     * @param fxml the fxml file to be loaded.
     */
    public static void loadFotoVista(String fxml) {
        try {
            final URL url = PaneNavigator.class.getResource(fxml);
            final Node node = FXMLLoader.load(url);
            mainController.setFotoVista(node);
        } 
        catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static void loadUserVista(String fxml) {
        try {
            final URL url = PaneNavigator.class.getResource(fxml);
            final Node node = FXMLLoader.load(url);
            mainController.setUserVista(node);
        } 
        catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

}
