/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.service;

import javafx.scene.Node;

/**
 *
 * @author simon
 */
public interface IAdminController extends IController {
            
    public void setUserVista(Node node);

    public void setFotoVista(Node node);
    
}
