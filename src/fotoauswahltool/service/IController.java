/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.service;

import javafx.application.Application;
import javafx.fxml.Initializable;

/**
 *
 * @author simon
 */
public interface IController extends Initializable {
    
    void setApp(final Application app);
    
}
