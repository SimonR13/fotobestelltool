/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoauswahltool.service.factory;

import fotoauswahltool.bean.BenutzerBean;
import fotoauswahltool.bean.BildBean;
import hib.dao.DaoFactory;
import hib.model.Benutzer;
import hib.model.Bild;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author simon
 */
public class HibernateBeanFactory {

    private static BildBean getEmptyBildBean() {
        return(new BildBean());
    }
    
    private static BenutzerBean getEmptyBenutzerBean() {
        return(new BenutzerBean());
    }
    
    public static BenutzerBean getBenutzerBeanFromBo(Benutzer bo) {
        final BenutzerBean bean = getEmptyBenutzerBean();
        
        bean.setLoginname(bo.getLoginname());
        bean.setId(bo.getId());
        bean.setName(bo.getName());
        bean.setVorname(bo.getVorname());
        bean.setIsAdmin(DaoFactory.getGruppeDao().isAdministrator(bo));
        bean.setResetPasswort(bo.getResetPasswort());
        
        return bean;
    }
    
    public static ObservableList<BenutzerBean> getBenutzerBeanList(final ObservableList<Benutzer> benutzerList) {
        final ObservableList<BenutzerBean> beanList = FXCollections.observableArrayList();
        for (Benutzer benutzer : benutzerList) {
            beanList.add(getBenutzerBeanFromBo(benutzer));
        }
        return beanList;
    }
    
    public static ObservableList<BildBean> getBildBeanList(final ObservableList<Bild> bilderList) {
        final ObservableList<BildBean> beanList = FXCollections.observableArrayList();
        for (Bild bild : bilderList) {
            beanList.add(getBildBeanFromBo(bild));
        }
        return beanList;
    }
    
    public static BildBean getBildBeanFromBo(final Bild bildBo) {
        final BildBean bean = getEmptyBildBean();
        bean.setName(bildBo.getName());
        bean.setPfad(bildBo.getDateiOrdner());
        bean.setDateiName(bildBo.getDateiName());
        return bean;
    }
    
    public static BildBean createBildBean(String dateiName, String pfad) {
        final BildBean bean = getEmptyBildBean();
        bean.setDateiName(dateiName);
        bean.setPfad(pfad);
        return bean;
    }
}
