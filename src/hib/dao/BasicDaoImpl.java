/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.dao;

import java.io.Serializable;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author simon
 * @param <T>
 * @param <ID>
 */
public class BasicDaoImpl<T, ID extends Serializable> implements IBasicDao<T, ID>{

    protected Class<T> clazz;
    protected SessionFactory factory;
    
    protected Transaction transaction;
    protected boolean handleTransaction = true;

    public BasicDaoImpl(Class<T> clazz, SessionFactory factory) {
        this.clazz = clazz;
        this.factory = factory;
    }

    @Override
    public Session getSession() {
        return factory.getCurrentSession();
    }

    @Override
    public Transaction getTransaction() {
        return this.transaction;
    }

    @Override
    public void openTransaction() {
        Session session = getSession();
        
        if (session.getTransaction().getStatus().isOneOf(TransactionStatus.ACTIVE)) {
            handleTransaction = false;
        }
        
        if (handleTransaction) {
            this.transaction = session.beginTransaction();
        }
    }

    @Override
    public void closeTransaction() {
        this.transaction.commit();
    }

    @Override
    public void rollbackTransaction() {
        if (handleTransaction) {
            this.transaction.rollback();
        }
    }

    @Override
    public T findById(Serializable id) {
        T result = null;
        
        openTransaction();
        
        try {
            result = (T) getSession().get(this.clazz, id);

            if (handleTransaction) {
                closeTransaction();
            }
        }
        catch (Exception e) {
            System.err.println("Es ist ein Fehler bei findByID  aufgetreten! Exception: " + e.getMessage());
            
            rollbackTransaction();
        }
        
        return result;
    }

    @Override
    public void save(T object) {
        openTransaction();
        
        try {
            getSession().save(object);
            
            if (handleTransaction) {
                closeTransaction();
            }
            else {
                getSession().flush();
            }
        }
        catch (Exception e) {
            System.err.println("Fehler beim Speichern von Objekt! Exception: " + e.getMessage());
            
            rollbackTransaction();
        }
    }

    @Override
    public void update(T object) {
        openTransaction();
        
        try {
            getSession().update(object);
            
            if (handleTransaction) {
                closeTransaction();
            }
        }
        catch (Exception e) {
            System.err.println("Fehler beim Aktualisieren von Objekt! Exception: " + e.getMessage());
            
            rollbackTransaction();
        }
    }

    @Override
    public void saveOrUpdate(T object) {
        openTransaction();
        
        try {
            getSession().saveOrUpdate(object);
            
            if (handleTransaction) {
                closeTransaction();
            }
        }
        catch (Exception e) {
            System.err.println("Fehler beim Speichern oder Aktualisieren von Objekt! Exception: " + e.getMessage());
            
            rollbackTransaction();
        }
    }

    @Override
    public T merge(T object) {
        T retVal = null;
        
        openTransaction();
        
        try {
            retVal = (T) getSession().merge(object);
            
            if (handleTransaction) {
                closeTransaction();
            }
        }
        catch (Exception e) {
            System.err.println("Fehler beim Merge von Objekten! Exception: " + e.getMessage());
            
            rollbackTransaction();
        }
        
        return retVal;
    }

    @Override
    public void delete(T object) {
        openTransaction();
        
        try {
            getSession().delete(object);
            
            if (handleTransaction) {
                closeTransaction();
            }
        }
        catch (Exception e) {
            System.err.println("Fehler beim Loeschen von Objekt! Exception: " + e.getMessage());
            
            rollbackTransaction();
        }
    }
    
}
