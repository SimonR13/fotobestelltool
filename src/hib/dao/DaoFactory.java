/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.dao;

import hib.model.dao.BenutzerDao;
import hib.HibernateSessionFactory;
import hib.model.dao.BildDao;
import hib.model.dao.BildGroessePreisDao;
import hib.model.dao.BilderAuswahlDao;
import hib.model.dao.GruppeDao;

/**
 *
 * @author simon
 */
public class DaoFactory {
    
    public static BenutzerDao getBenutzerDao() {
        return(new BenutzerDao(HibernateSessionFactory.getInstance()));
    }
    
    public static GruppeDao getGruppeDao() {
        return(new GruppeDao(HibernateSessionFactory.getInstance()));
    }

    public static BildGroessePreisDao getBildGroessePreisDao() {
        return(new BildGroessePreisDao(HibernateSessionFactory.getInstance()));
    }
    
    public static BilderAuswahlDao getBilderAuswahlDao() {
        return(new BilderAuswahlDao(HibernateSessionFactory.getInstance()));
    }

    public static BildDao getBildDao() {
        return(new BildDao(HibernateSessionFactory.getInstance()));
    }
    
}
