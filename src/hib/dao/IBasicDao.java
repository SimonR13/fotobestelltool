/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.dao;

import java.io.Serializable;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author simon
 * @param <T>
 */
public interface IBasicDao<T, ID extends Serializable> {
    
    public Session getSession();

    public Transaction getTransaction();
    
    public void openTransaction();
    
    public void closeTransaction();
    
    public void rollbackTransaction();
    
    public T findById(ID id);
    
    public void save(T object);
    
    public void update(T object);

    public void saveOrUpdate(T object);
    
    public T merge(T object);

    public void delete(T object);
    
}
