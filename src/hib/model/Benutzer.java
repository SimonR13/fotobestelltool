/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author simon
 */

@Entity
@Table(name = "BENUTZER")
public class Benutzer extends Bo {
    
    private String name;
    private String vorname;
    private String loginname;
    private String passwort;
    
    @Transient
    private Boolean resetPasswort;
    
    @ManyToOne
    @JoinColumn(name = "GRUPPE")
    private Gruppe gruppe;

    @OneToMany(mappedBy = "ersteller")
    private List<Bestellung> bestellungen;
    
    @OneToMany(mappedBy = "besteller")
    private List<BestellungBilder> bestellungBilder;
    
    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gruppe getGruppe() {
        return gruppe;
    }

    public void setGruppe(Gruppe gruppe) {
        this.gruppe = gruppe;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public List<Bestellung> getBestellungen() {
        return bestellungen;
    }

    public void setBestellungen(List<Bestellung> bestellungen) {
        this.bestellungen = bestellungen;
    }

    public List<BestellungBilder> getBestellungBilder() {
        return bestellungBilder;
    }

    public void setBestellungBilder(List<BestellungBilder> bestellungBilder) {
        this.bestellungBilder = bestellungBilder;
    }

    public Boolean getResetPasswort() {
        return resetPasswort;
    }

    public void setResetPasswort(Boolean resetPasswort) {
        this.resetPasswort = resetPasswort;
    }
    
    public String getKonkreterName() {
        return(getVorname() + " " + getName());
    }
    
    @Override
    public String toString() {
        return("Benutzer{" + "name=" + name + ", vorname=" + vorname + ", loginname=" + loginname + ", passwort=" + passwort + ", gruppe=" + gruppe + "}");
    }
    
}
