/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author simon
 */

@Entity
@Table(name = "BESTELLUNGEN")
public class Bestellung extends Bo {
    
    private String bezeichnung;
    
    @ManyToOne
    @JoinColumn(name = "BENUTZER")
    private Benutzer ersteller;
    
    @OneToOne
    private BestellungBilder bestellungBilder;

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public Benutzer getErsteller() {
        return ersteller;
    }

    public void setErsteller(Benutzer ersteller) {
        this.ersteller = ersteller;
    }

    public BestellungBilder getBestellungBilder() {
        return bestellungBilder;
    }

    public void setBestellungBilder(BestellungBilder bestellungBilder) {
        this.bestellungBilder = bestellungBilder;
    }

    @Override
    public String toString() {
        return("Bestellung{" + "bezeichnung=" + bezeichnung + ", ersteller=" + ersteller + ", bestellungBilder=" + bestellungBilder + "}");
    }
    
}