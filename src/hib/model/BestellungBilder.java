/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author simon
 */

@Entity
@Table(name = "BESTELLUNGEN_BILDER")
public class BestellungBilder extends Bo {
    
    private Integer anzahl;

    @ManyToOne
    @JoinColumn(name = "BESTELLER", nullable = false)
    private Benutzer besteller;
            
    @ManyToOne
    @JoinColumn(name = "BESTELLUNG", nullable = false)
    private Bestellung bestellung;
    
    @ManyToOne
    @JoinColumn(name = "BILD", nullable = false)
    private Bild bild;

    public Integer getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(Integer anzahl) {
        this.anzahl = anzahl;
    }

    public Benutzer getBesteller() {
        return besteller;
    }

    public void setBesteller(Benutzer besteller) {
        this.besteller = besteller;
    }

    public Bestellung getBestellung() {
        return bestellung;
    }

    public void setBestellung(Bestellung bestellung) {
        this.bestellung = bestellung;
    }

    public Bild getBild() {
        return bild;
    }

    public void setBild(Bild bild) {
        this.bild = bild;
    }

    @Override
    public String toString() {
        return("BestellungBilder{" + "anzahl=" + anzahl + ", besteller=" + besteller + ", bestellung=" + bestellung + ", bild=" + bild + "}");
    }

}
