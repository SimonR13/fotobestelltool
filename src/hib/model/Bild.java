/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author simon
 */

@Entity
@Table(name = "BILDER")
public class Bild extends Bo {
    
    private String name;
    private String dateiOrdner;
    private String dateiName;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateiOrdner() {
        return dateiOrdner;
    }

    public void setDateiOrdner(String dateiOrdner) {
        this.dateiOrdner = dateiOrdner;
    }

    public String getDateiName() {
        return dateiName;
    }

    public void setDateiName(String dateiName) {
        this.dateiName = dateiName;
    }
    
    @Override
    public String toString() {
        return("Bild{" + "name=" + name + ", dateiOrdner=" + dateiOrdner + ", dateiName=" + dateiName + "}");
    }

}
