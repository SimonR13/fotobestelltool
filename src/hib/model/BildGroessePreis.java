/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author simon
 */
@Entity
@Table(name="BILD_GROESSE_PREIS")
public class BildGroessePreis extends Bo {
    
    private Integer breite;
    private Integer hoehe;
    
    private Double preis;

    public Integer getBreite() {
        return breite;
    }

    public void setBreite(Integer breite) {
        this.breite = breite;
    }

    public Integer getHoehe() {
        return hoehe;
    }

    public void setHoehe(Integer hoehe) {
        this.hoehe = hoehe;
    }

    public Double getPreis() {
        return preis;
    }

    public void setPreis(Double preis) {
        this.preis = preis;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Bildgroesse:" + getBreite() + "x" + getHoehe());
        builder.append("Preis:" + getPreis());
        return builder.toString();
    }
    
}
