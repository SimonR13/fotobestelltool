/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author simon
 */
@Entity
@Table(name="BILDER_AUSWAHL")
public class BilderAuswahl extends Bo {
    
    private String name;
    
    @Temporal(TemporalType.DATE)
    private Date gueltigBis;
    
    @ManyToMany
    @JoinTable(name = "BILD_BILDER_AUSWAHL", 
            joinColumns = @JoinColumn(name = "BILDER_AUSWAHL"),
            inverseJoinColumns = @JoinColumn(name = "BILD"))
    private Map<Long, Bild> bilder;
    
    @OneToMany
    @JoinTable(name = "BILDER_AUSWAHL_GRUPPE",
            joinColumns = @JoinColumn(name = "BILDER_AUSWAHL"),
            inverseJoinColumns = @JoinColumn(name = "GRUPPE"))
    private List<Gruppe> gruppen;
    
    @OneToMany
    @JoinColumn(name = "BESTELLUNG")
    private List<Bestellung> bestellungen;
    
    @OneToOne(optional = true)
    private Abrechnung abrechnung;
    
    @OneToMany
    @JoinColumn(name="BILD_GROESSE_PREIS")
    private List<BildGroessePreis> bildGroessePreis;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getGueltigBis() {
        return gueltigBis;
    }

    public void setGueltigBis(Date gueltigBis) {
        this.gueltigBis = gueltigBis;
    }

    public Map<Long, Bild> getBilder() {
        return bilder;
    }

    public void setBilder(Map<Long, Bild> bilder) {
        this.bilder = bilder;
    }

    public List<Bestellung> getBestellungen() {
        return bestellungen;
    }

    public void setBestellungen(List<Bestellung> bestellungen) {
        this.bestellungen = bestellungen;
    }

    public Abrechnung getAbrechnung() {
        return abrechnung;
    }

    public void setAbrechnung(Abrechnung abrechnung) {
        this.abrechnung = abrechnung;
    }

    public List<Gruppe> getGruppen() {
        return gruppen;
    }

    public void setGruppen(List<Gruppe> gruppen) {
        this.gruppen = gruppen;
    }

    public List<BildGroessePreis> getBildGroessePreis() {
        return bildGroessePreis;
    }

    public void setBildGroessePreis(List<BildGroessePreis> bildGroessePreis) {
        this.bildGroessePreis = bildGroessePreis;
    }

}
