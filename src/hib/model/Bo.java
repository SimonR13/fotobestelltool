/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author simon
 */

@MappedSuperclass
public class Bo implements Serializable {

    public Bo() {
        this.crea = new Date();
        this.lmod = new Date();
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Temporal(TemporalType.TIMESTAMP)
    protected Date crea;
    
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lmod;

    @PrePersist
    protected void onCreate() {
        setCrea(new Date());
        setLmod(getCrea());
    }
    
    @PreUpdate
    protected void onUpdate() {
        setLmod(new Date());
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCrea() {
        return crea;
    }

    public void setCrea(Date crea) {
        this.crea = crea;
    }

    public Date getLmod() {
        return lmod;
    }

    public void setLmod(Date lmod) {
        this.lmod = lmod;
    }
    
}
