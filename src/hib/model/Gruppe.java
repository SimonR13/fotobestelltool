/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author simon
 */

@Entity
@Table(name = "GRUPPEN")
public class Gruppe extends Bo {

    private String name;
    
    private String beschreibung;
    
    @OneToMany(mappedBy = "gruppe")
    private List<Benutzer> benutzer;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "GRUPPEN_RECHTE",
            joinColumns = @JoinColumn(name = "gruppe_fk"),
            inverseJoinColumns = @JoinColumn(name = "rechte_fk"))
    private List<Recht> rechte;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public List<Benutzer> getBenutzer() {
        return benutzer;
    }

    public void setBenutzer(List<Benutzer> benutzer) {
        this.benutzer = benutzer;
    }

    public List<Recht> getRechte() {
        return rechte;
    }

    public void setRechte(List<Recht> rechte) {
        this.rechte = rechte;
    }

    @Override
    public String toString() {
        return("Gruppe{" + "name=" + name + ", beschreibung=" + beschreibung + "}");
    }

}
