/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author simon
 */

@Entity
@Table(name = "RECHTE")
public class Recht extends Bo {
    
    private String name;
    
    private String beschreibung;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }
    
}
