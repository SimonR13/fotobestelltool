/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model.dao;

import hib.dao.BasicDaoImpl;
import hib.model.Benutzer;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.SessionFactory;

/**
 *
 * @author simon
 */
public class BenutzerDao extends BasicDaoImpl<Benutzer, Long> {

    public BenutzerDao(SessionFactory factory) {
        super(Benutzer.class, factory);
    }
    
    public Benutzer getBenutzerByLoginname(final String name) {
        Benutzer retVal = null;
        
        openTransaction();
        try {
            final List<Benutzer> resultList = getSession()
                    .createQuery("FROM Benutzer WHERE LOWER(loginname) LIKE :name")
                    .setString("name", name.toLowerCase())
                    .list();
            
            if (resultList != null && resultList.size() == 1) {
                retVal = resultList.get(0);
            }
            else if (resultList != null && resultList.size() > 1) {
                retVal = null;
                System.err.println("Beim Laden nach Benutzer wurden mehrere gleiche Benutzer gefunden!");
            }
            
            if (handleTransaction) {
                closeTransaction();
            }
        }
        catch (Exception e) {
            rollbackTransaction();
            System.err.println("Fehler beim Laden des Benutzers! Exception: " + e.getMessage());
        }
        
        return retVal;
    }
    
    public ObservableList<Benutzer> getBenutzerList() {
        final ObservableList<Benutzer> userList = FXCollections.observableArrayList();
        
        openTransaction();
        try {
            final List<Benutzer> tmpList = getSession()
                    .createQuery("FROM Benutzer")
                    .list();
            
            if ((tmpList != null) && !(tmpList.isEmpty())) {
                userList.addAll(tmpList);
            }
            
            if (handleTransaction) {
                closeTransaction();
            }
        }
        catch (Exception e) {
            rollbackTransaction();
            System.err.println("Exception: " + e.getMessage());
        }
        
        return userList;
    }

}
