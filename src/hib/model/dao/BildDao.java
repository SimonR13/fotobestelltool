/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model.dao;

import hib.dao.BasicDaoImpl;
import hib.model.Bild;
import org.hibernate.SessionFactory;

/**
 *
 * @author simon
 */
public class BildDao extends BasicDaoImpl<Bild, Long> {
    
    public BildDao(SessionFactory factory) {
        super(Bild.class, factory);
    }
    
}
