/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model.dao;

import hib.dao.BasicDaoImpl;
import hib.model.BildGroessePreis;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;

/**
 *
 * @author simon
 */
public class BildGroessePreisDao extends BasicDaoImpl<BildGroessePreis, Long> {
    
    public BildGroessePreisDao(SessionFactory factory) {
        super(BildGroessePreis.class, factory);
    }
    
    public List<BildGroessePreis> getDistinctGroessen() {
        List<BildGroessePreis> retVal = new ArrayList<>();
        
        openTransaction();
        try {
            final List<Object> tmpList = getSession()
                    .createCriteria(BildGroessePreis.class)       
                    .setProjection(Projections.projectionList()
                            .add(Projections.groupProperty("breite"))
                            .add(Projections.groupProperty("hoehe"))
                            .add(Projections.groupProperty("preis"))
                    )
                    .list();
            
            for (Object obj : tmpList) {
                final Object[] objArray = (Object[]) obj;
                BildGroessePreis tmp = new BildGroessePreis();
                tmp.setBreite((Integer) objArray[0]);
                tmp.setHoehe((Integer) objArray[1]);
                tmp.setPreis((Double) objArray[2]);
                retVal.add(tmp);
            }
            
            if (handleTransaction) {
                closeTransaction();
            }
        }
        catch (Exception e) {
            rollbackTransaction();
            System.err.println("Fehler beim Laden der verschiedenen Groessen! Exception: " + e.getMessage());
        }
        
        return retVal;
    }
    
}
