/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hib.model.dao;

import hib.HibernateConstants;
import hib.dao.BasicDaoImpl;
import hib.model.Benutzer;
import hib.model.Gruppe;
import hib.model.Recht;
import org.hibernate.SessionFactory;

/**
 *
 * @author simon
 */
public class GruppeDao extends BasicDaoImpl<Gruppe, Long> {

    public GruppeDao(SessionFactory factory) {
        super(Gruppe.class, factory);
    }
    
    public Gruppe getGruppeByName(final String name) {
        Gruppe retVal = null;
        
        openTransaction();
        try {
            retVal = (Gruppe) getSession().createQuery("FROM Gruppe WHERE LOWER(name) LIKE :name")
                    .setString("name", "%" + name.toLowerCase() + "%")
                    .uniqueResult();
            
            if (handleTransaction) {
                closeTransaction();
            }
        }
        catch (Exception e) {
            System.err.println("Fehler bei getGruppeByName()! Exception: " + e.getMessage());
        }
        
        return retVal;
    }
    
    public Gruppe getAdminGruppe() {
        return(getGruppeByName(HibernateConstants.ADMIN_RIGHTS));
    }
    
    public Gruppe getBenutzerGruppe() {
        return(getGruppeByName(HibernateConstants.USER_RIGHTS));
    }
    
    public boolean isAdministrator(final Benutzer user) {
        boolean retVal = Boolean.FALSE;
        
        openTransaction();
        
        if (user.getGruppe() != null) {
            for (Recht rights : user.getGruppe().getRechte()) {
                if (HibernateConstants.ADMIN_RIGHTS.equalsIgnoreCase(rights.getName())) {
                    retVal = Boolean.TRUE;
                    break;
                }
            }
        }
        
        if (handleTransaction) {
            closeTransaction();
        }
        
        return retVal;
    }
}
