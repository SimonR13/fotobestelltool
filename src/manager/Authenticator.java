/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import hib.dao.DaoFactory;
import hib.model.Benutzer;
import java.util.HashMap;
import java.util.Map;
import utils.StringUtils;

/**
 *
 * @author simon
 */
public class Authenticator {
    private static final Map<String, Benutzer> USERS = new HashMap<>();
    
    public static Benutzer validate(String user, String password) {
        Benutzer retVal = null;
        if (!USERS.containsKey(user)) {
            final Benutzer benutzer = DaoFactory.getBenutzerDao().getBenutzerByLoginname(user);
            
            USERS.put(user, benutzer);
            
            if (checkPassword(benutzer.getPasswort(), password)) {
                retVal = benutzer;
            }
        
        }
        else {
            final Benutzer benutzer = USERS.get(user);
            if (checkPassword(benutzer.getPasswort(), password)) {
                retVal = benutzer;
            }
        }
        
        return retVal;
    }
    
    private static boolean checkPassword(String entered, String expected) {
        if (StringUtils.isNotBlank(entered) && StringUtils.isNotBlank(expected)) {
            return(entered.equals(expected));
        }
        return false;
    }
}
