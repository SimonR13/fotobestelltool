/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author simon
 */
public class DatabaseManager {
    public static void close() {
        try {
            DriverManager.getConnection("jdbc:derby:FotoToolDatabase;user=root;password=toor;shutdown=true");
        } 
        catch (SQLException se) {
            // All good, connection successfully closed
        }
    } 
}
