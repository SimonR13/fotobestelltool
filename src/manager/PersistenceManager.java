/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import fotoauswahltool.bean.BildBean;
import fotoauswahltool.bean.BilderAuswahlBean;
import fotoauswahltool.bean.BreiteHoehePreisBean;
import hib.dao.DaoFactory;
import hib.model.Bild;
import hib.model.BildGroessePreis;
import hib.model.BilderAuswahl;
import hib.model.dao.BildDao;
import hib.model.dao.BildGroessePreisDao;
import hib.model.dao.BilderAuswahlDao;
import java.util.Date;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author simon
 */
public class PersistenceManager {
    
    public static boolean persistBreiteHoehePreisListe(final ObservableList<BreiteHoehePreisBean> breiteHoehePreisList) {
        if (breiteHoehePreisList != null && !(breiteHoehePreisList.isEmpty())) {
            final BildGroessePreisDao dao = DaoFactory.getBildGroessePreisDao();
            final List<BildGroessePreis> distinctEntries = dao.getDistinctGroessen();
            for (BreiteHoehePreisBean bean : breiteHoehePreisList) {
                if (distinctEntries != null) {
                    boolean exists = false;
                    for (BildGroessePreis entry : distinctEntries) {
                        if(exists = (entry.getBreite().equals(bean.getBreite())
                                && entry.getHoehe().equals(bean.getHoehe())
                                && entry.getPreis().equals(bean.getPreis()))) {
                            break;
                        }
                    }
                    if (exists) {
                        continue;
                    }
                }
                try {
                    final BildGroessePreis bo = new BildGroessePreis();
                    bo.setBreite(bean.getBreite());
                    bo.setHoehe(bean.getHoehe());
                    bo.setPreis(bean.getPreis());
                    
                    dao.save(bo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static boolean persistNeueAuswahl(BilderAuswahlBean neueBilderAuswahl, ObservableList<BreiteHoehePreisBean> BREITE_HOEHE_PREIS_LISTE) {
        if (persistBreiteHoehePreisListe(BREITE_HOEHE_PREIS_LISTE)) {
            final BilderAuswahlDao auswahlDao = DaoFactory.getBilderAuswahlDao();
            final BildDao bildDao = DaoFactory.getBildDao();
            final BilderAuswahl newAuswahl = new BilderAuswahl();
            newAuswahl.setGueltigBis(new Date(newAuswahl.getGueltigBis().getTime()));
            newAuswahl.setName(neueBilderAuswahl.getName());
            for (BildBean bean : neueBilderAuswahl.getBilder()) {
                final Bild newBild = new Bild();
                newBild.setDateiName(bean.getDateiName());
                newBild.setDateiOrdner(bean.getPfad());
                newBild.setName(bean.getName());
                bildDao.save(newBild);
                newAuswahl.getBilder().put(newBild.getId(), newBild);
            }
            auswahlDao.save(newAuswahl);
            return true;
        }
        return false;
    }
    
}
