/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 *
 * @author simon
 */
public class StringUtils {
    private SecureRandom random = new SecureRandom();
    
    public static boolean isBlank(final String str) {
        if (str == null) {
            return(Boolean.TRUE);
        }
        
        if (str.trim().length() <= 0 && str.trim().equals("")) {
            return(Boolean.TRUE);
        }
        
        return(Boolean.FALSE);
    }
    
    public static boolean isNotBlank(final String str) {
        if (str == null) {
            return false;
        }
        
        if (str.trim().length() > 0) {
            return true;
        }
        
        return false;
    }
    
    public static boolean isNotEmpty(final String str) {
        if (str == null) {
            return false;
        }
        
        if (str.length() > 0) {
            return true;
        }
        
        return false;
    }
    
    public String nextSessionId() {
        return new BigInteger(130, random).toString(32);
    }
    
}
