/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import hib.model.Gruppe;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

/**
 *
 * @author simon
 */
public class DBTest {
    
    @Test
    public void testDB() {
        final Session session = initHibernate();
        try {
            Transaction tx = session.beginTransaction();
            try {
//                create(session);
                list(session);

                tx.commit();
                tx = null;
            }
            finally {
                if (tx != null) {
                    tx.rollback();
                    tx = null;
                }
            }
        }
        finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    private void create(final Session session) {
        Gruppe n = new Gruppe();
        n.setName("Admin");
        session.save(n);

        n = new Gruppe();
        n.setName("Benutzer");
        session.save(n);

        n = new Gruppe();
        n.setName("Test");
        session.save(n);
    }

    private void list(final Session session) {
        Query q = session.createQuery("from " + Gruppe.class.getName());
        List<Gruppe> list = q.list();
        System.out.println("Query came back with " + list.size() + " results");
        for (Gruppe row : list) {
            System.out.print("GruppenId: " + row.getId() + " - ");
            System.out.println(row);
        }
    }

    public Session initHibernate() {
        final Configuration configuration = new Configuration(). configure("hibernate.cfg.xml");
        System.out.println("Connecting hibernate to URL=" + configuration.getProperty("hibernate.connection.url")
                 + " as user=" + configuration.getProperty("hibernate.connection.username"));
        return configuration.buildSessionFactory().getCurrentSession();
    }
}
