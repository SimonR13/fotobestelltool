/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.hib;

import hib.model.Benutzer;
import hib.model.Gruppe;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author simon
 */
public class TestHibernate {
    private static SessionFactory sessionFactory;
    
    @BeforeClass
    public static void setUp() {
        // A SessionFactory is set up once for an application!
	final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
			.configure() // configures settings from hibernate.cfg.xml
			.build();
	try {
            sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
	}
	catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy( registry );
	}
    }
    
    public void testWriteGruppeDb() {
        Gruppe gruppe1 = saveGruppe("Sumith");
        Gruppe gruppe2 = saveGruppe("Anoop");
        System.out.println("After Successfully insertion ");
        System.out.println(gruppe1);
        System.out.println(gruppe2);
        listGruppen();
       
        sessionFactory.close();
    }
    
    @Test
    public void testWriteBenutzerDb() {
        Gruppe sumithGruppe = readGruppe("Admin");
        System.out.println("Start: testWriteBenutzer");
        Benutzer benutzer1 = saveBenutzer(sumithGruppe, "Sumith", "Akon", "akon");
        Benutzer benutzer2 = saveBenutzer(sumithGruppe, "Sumith", "Bkon", "bkon");
        System.out.println("Start: testWriteBenutzer");
        listBenutzer();
    }
    
    @Test
    public void testReadDb() {
        System.out.println("Start Test");
        System.out.println("Start listGruppen()");
        listGruppen();
        System.out.println("Ende listGruppen()");
        System.out.println("Start listBenutzer()");
        listBenutzer();
        System.out.println("Ende listBenutzer()");
        System.out.println("End Test");
    }
    
    public Gruppe saveGruppe(String gruppeName) {
        Session sess = sessionFactory.getCurrentSession();
        Transaction tx = null;
        Gruppe gruppe = new Gruppe();
        try {
            tx = sess.beginTransaction();
            
            gruppe.setName(gruppeName);
            gruppe = (Gruppe) sess.merge(gruppe);
            tx.commit();
        } 
        catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
        }
        
        return gruppe;
    } 
    
    public Benutzer saveBenutzer(Gruppe gruppe, String benutzerName, String benutzerVorname,
            String loginName) {
        Session sess = sessionFactory.getCurrentSession();
        Transaction tx = null;
        Benutzer ben = new Benutzer();
        try {
            tx = sess.beginTransaction();
            
            ben.setLoginname(loginName);
            ben.setName(benutzerName);
            ben.setVorname(benutzerVorname);
            ben.setGruppe(gruppe);
            
            ben = (Benutzer) sess.merge(ben);
            tx.commit();
        } 
        catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
        }
        
        return ben;
    } 
    
    public void listGruppen() {
        Session sess = sessionFactory.getCurrentSession();
        Transaction tx = sess.beginTransaction();
        try {
            final List<Gruppe> gruppeList = sess.createQuery("from Gruppe").list();
            for (Iterator<Gruppe> iterator = gruppeList.iterator(); iterator.hasNext();) {
              Gruppe gruppe = (Gruppe) iterator.next();
              System.out.println(gruppe);
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            System.err.println("Fehler bei listGruppe()! Exception: " + e.getMessage());
        }
    }
    
    public void listBenutzer() {
        Session sess = sessionFactory.getCurrentSession();
        Transaction tx = sess.beginTransaction();
        
        try {
            final List<Benutzer> benutzerList = sess.createQuery("from Benutzer").list();
            for (Iterator<Benutzer> iter = benutzerList.iterator(); iter.hasNext();) {
                Benutzer benutzer = (Benutzer) iter.next();
                System.out.print("BenutzerId: " + benutzer.getId() + " ");
                System.out.println(benutzer);
            }
            tx.commit();
        }
        catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            System.err.println("Fehler bei listBenutzer()! Exception: " + e.getMessage());
        }
    }

    private Gruppe readGruppe(String name) {
        Session sess = sessionFactory.getCurrentSession();
        sess.beginTransaction();
        
        Gruppe retVal = null;
        try {
            retVal = (Gruppe) sess.createQuery("from Gruppe WHERE LOWER(name) LIKE LOWER(:name)")
                    .setString("name", "%" + name + "%")
                    .uniqueResult();
            
            sess.getTransaction().commit();
        }
        catch (Exception e) {
            sess.getTransaction().rollback();
            System.err.println("Fehler bei readGruppe(" + name + ")! Exception: " + e.getMessage());
        }
        
        return retVal;
    }
}
