/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.hib;

import hib.HibernateSessionFactory;
import hib.model.Gruppe;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author simon
 */
public class TestHibernateSession {
    private static SessionFactory FACTORY;
    private static Session sess;
    
    @BeforeClass
    public static void setUpClass() {
        FACTORY = HibernateSessionFactory.getInstance();
    }
    
    @Test
    public void testSession() {
        Transaction tx = null;

        sess = FACTORY.getCurrentSession();
        try {
            tx = sess.beginTransaction();
            
            final List<Gruppe> gruppen = sess.createQuery("from Gruppe").list();
            for (Gruppe gruppe : gruppen) {
                System.out.println(gruppe);
            }
            
            tx.commit();
        }
        catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
        }
        
    }
    
}
