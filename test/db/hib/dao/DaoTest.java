package db.hib.dao;

import hib.dao.DaoFactory;
import hib.model.Benutzer;
import hib.model.BildGroessePreis;
import hib.model.dao.BenutzerDao;
import hib.model.dao.BildGroessePreisDao;
import java.util.List;
import manager.DatabaseManager;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author simon
 */
public class DaoTest {
    
    private static final Long TEST_BENUTZER_ID = 1116L;
    private static final String TEST_BENUTZER_NAME = "bkon";
    private static final Long TEST_GRUPPE_ID = 5L;
    private static final String TEST_GRUPPE_NAME = "Anschauer";
    
    @Test
    public void testGruppeDaoId() {
        Assert.assertNotNull(DaoFactory.getGruppeDao().findById(TEST_GRUPPE_ID));
    }    
    @Test
    public void testGruppeDaoName() {
        Assert.assertNotNull(DaoFactory.getGruppeDao().getGruppeByName(TEST_GRUPPE_NAME));
    }
    
    @Test
    public void testBenutzerDaoId() {
        Assert.assertNotNull(DaoFactory.getBenutzerDao().findById(TEST_BENUTZER_ID));
    }
    
    @Test
    public void testBenutzerDaoName() {
        Assert.assertNotNull(DaoFactory.getBenutzerDao().getBenutzerByLoginname(TEST_BENUTZER_NAME));
    }
    
    public void testAddBenutzer() {
        final BenutzerDao dao = DaoFactory.getBenutzerDao();
        final Benutzer newUser = new Benutzer();
        newUser.setName("Ritzel");
        newUser.setVorname("Simon");
        newUser.setLoginname("ritzels");
        newUser.setPasswort("@sr1!");
        newUser.setResetPasswort(Boolean.TRUE);
        dao.save(newUser);
        
        final Benutzer test = dao.getBenutzerByLoginname("ritzels");
        Assert.assertNotNull(test);
    }
    
    @Test
    public void testShutdownDerby() {
        DatabaseManager.close();
    }
    
    @Test
    public void testAlleBildGroessePreis() {
        final BildGroessePreisDao dao = DaoFactory.getBildGroessePreisDao();
        BildGroessePreis bgp = new BildGroessePreis();
        bgp.setBreite(9);
        bgp.setHoehe(7);
        bgp.setPreis(0.15D);
        dao.save(bgp);
//        BildGroessePreis bgp3 = new BildGroessePreis();
//        bgp3.setBreite(9);
//        bgp3.setHoehe(6);
//        bgp3.setPreis(0.12D);
//        dao.save(bgp3);
        
        final List<?> resultList = dao.getDistinctGroessen();
        Assert.assertNotNull(resultList);
        Assert.assertFalse(resultList.isEmpty());
        for (Object bgp2 : resultList) {
            for (Object obj : ((Object[]) bgp2)) {
                System.out.println((Integer) obj);
            }
        }
    }
    
}
